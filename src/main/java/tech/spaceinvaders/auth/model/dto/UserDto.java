package tech.spaceinvaders.auth.model.dto;

import tech.spaceinvaders.auth.validation.ValidateEmail;
import tech.spaceinvaders.auth.validation.MatchPassword;
import tech.spaceinvaders.auth.validation.ValidatePassword;
import tech.spaceinvaders.auth.validation.ValidateTerms;

@MatchPassword
public class UserDto {

//    @NotNull
//    @NotEmpty
//    private String firstName;
//
//    @NotNull
//    @NotEmpty
//    private String lastName;

    @ValidatePassword
    private String password;

    private String duplicatedPassword;

    @ValidateEmail
    private String email;

    @ValidateTerms
    private Boolean terms;

//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDuplicatedPassword() {
        return duplicatedPassword;
    }

    public void setDuplicatedPassword(String duplicatedPassword) {
        this.duplicatedPassword = duplicatedPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getTerms() {
        return terms;
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }
}