package tech.spaceinvaders.auth.model.dto;

import javax.validation.constraints.NotBlank;

public class TokenDto {

    @NotBlank(message = "Token is mandatory parameter")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
