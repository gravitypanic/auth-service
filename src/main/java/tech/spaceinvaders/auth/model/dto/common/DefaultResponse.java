package tech.spaceinvaders.auth.model.dto.common;

import com.fasterxml.jackson.annotation.*;
import tech.spaceinvaders.auth.error.entities.ErrorCode;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DefaultResponse {

    private String message;
    private Integer errorCode;

    public DefaultResponse(ErrorCode code) {
        new DefaultResponse(code.getCode(), code.getMessage());
    }

    public DefaultResponse(ErrorCode code, String message) {
        new DefaultResponse(code.getCode(), message);
    }

    public DefaultResponse(String message) {
        this.message = message;
    }

    public DefaultResponse(Integer error, String message) {
        this.errorCode = error;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
