package tech.spaceinvaders.auth.model.dto;

import tech.spaceinvaders.auth.validation.ValidateCurrentPassword;
import tech.spaceinvaders.auth.validation.ValidatePassword;

public class PasswordDto {

    @ValidatePassword
    private String newPassword;
    @ValidateCurrentPassword
    private String currentPassword;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
