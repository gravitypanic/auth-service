package tech.spaceinvaders.auth.model.dto;

import javax.validation.constraints.NotBlank;

public class EmailDto {

    @NotBlank(message = "Email is mandatory parameter")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
