package tech.spaceinvaders.auth.model.dao;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "verifications", uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id" }) })
public class Verification {

    private static final int TIME_LIMIT = 86400000;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "token")
    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiration_date")
    private Date expirationDate;

    public Verification() {}

    public Verification(final String token) {
        this.token = token;
        this.expirationDate = getExpirationDateCalculation();
    }

    public Verification(final User user, final String token) {
        this.token = token;
        this.user = user;
        this.expirationDate = getExpirationDateCalculation();
    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    private Date getExpirationDateCalculation() {
        return new Date(new Date().getTime() + TIME_LIMIT);
    }

    public void updateToken(final String token) {
        this.token = token;
        this.expirationDate = getExpirationDateCalculation();
    }

    @Override
    public int hashCode() {
        return 7 * ((token == null) ? 0 : token.hashCode()) + ((expirationDate == null) ? 0 : expirationDate.hashCode());
    }

    @Override
    public boolean equals(final Object obj) {
        if ((obj == null) || (getClass() != obj.getClass())) return false;
        final Verification verification = (Verification) obj;
        return (this.token.equals(verification.getToken())) &&
                (this.expirationDate.equals(verification.getExpirationDate()));
    }

    @Override
    public String toString() {
        return "Validation [token=".concat(token).concat(", expirationDate=")
                .concat(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(expirationDate)).concat("]");
    }

}
