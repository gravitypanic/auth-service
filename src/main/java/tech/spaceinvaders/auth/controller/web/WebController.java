package tech.spaceinvaders.auth.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @GetMapping("/validation")
    public String validation() {
        return "validation";
    }

    @GetMapping("/recovery")
    public String recovery() {
        return "recovery";
    }

    @GetMapping("/password")
    public String password() {
        return "password";
    }

}
