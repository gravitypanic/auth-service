package tech.spaceinvaders.auth.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;
import tech.spaceinvaders.auth.error.entities.UserDoesNotExistException;
import tech.spaceinvaders.auth.event.RecoveryEvent;
import tech.spaceinvaders.auth.model.dao.User;
import tech.spaceinvaders.auth.model.dto.EmailDto;
import tech.spaceinvaders.auth.model.dto.PasswordDto;
import tech.spaceinvaders.auth.model.dto.TokenDto;
import tech.spaceinvaders.auth.model.dto.common.DefaultResponse;
import tech.spaceinvaders.auth.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @PostMapping("/recovery")
    public DefaultResponse recovery(@Valid @RequestBody EmailDto emailDto) {
        User user = userService.findUserByEmail(emailDto.getEmail());
        if (user == null) throw new UserDoesNotExistException(emailDto.getEmail());

        eventPublisher.publishEvent(new RecoveryEvent(user));

        return new DefaultResponse("sent");
    }

    @PostMapping("/recovery/validate")
    public DefaultResponse validate(@Valid @RequestBody TokenDto tokenDto) {
        userService.validateRecoveryToken(tokenDto.getToken());

        return new DefaultResponse("validated");
    }

    @PostMapping("/password/change")
    public DefaultResponse change(@Valid @RequestBody PasswordDto passwordDto) {
        userService.changePassword(passwordDto.getNewPassword());

        return new DefaultResponse("changed");
    }

}