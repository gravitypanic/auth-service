package tech.spaceinvaders.auth.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;
import tech.spaceinvaders.auth.error.entities.UserDoesNotExistException;
import tech.spaceinvaders.auth.event.VerificationEvent;
import tech.spaceinvaders.auth.model.dao.User;
import tech.spaceinvaders.auth.model.dto.EmailDto;
import tech.spaceinvaders.auth.model.dto.TokenDto;
import tech.spaceinvaders.auth.model.dto.common.DefaultResponse;
import tech.spaceinvaders.auth.service.UserService;
import tech.spaceinvaders.auth.model.dto.UserDto;

import javax.validation.Valid;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @PostMapping
    public DefaultResponse registerUserAccount(@Valid @RequestBody UserDto userDto) {
        User user = userService.registerUser(userDto);
        eventPublisher.publishEvent(new VerificationEvent(user));

        return new DefaultResponse("success");
    }

    @PostMapping("/confirm")
    public DefaultResponse emailVerification(@Valid @RequestBody TokenDto tokenDto) {
        userService.validateVerificationToken(tokenDto.getToken());

        return new DefaultResponse("verified");
    }

    @PostMapping("/confirm/resend")
    public DefaultResponse resendEmailVerification(@Valid @RequestBody EmailDto emailDto) {
        User user = userService.findUserByEmail(emailDto.getEmail());
        if (user == null) throw new UserDoesNotExistException(emailDto.getEmail());

        eventPublisher.publishEvent(new VerificationEvent(user));

        return new DefaultResponse("sent");
    }

}