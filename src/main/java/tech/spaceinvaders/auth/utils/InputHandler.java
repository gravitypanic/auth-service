package tech.spaceinvaders.auth.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import tech.spaceinvaders.auth.error.entities.InvalidRequestBodyException;

import java.io.InputStream;


public class InputHandler {

    public static <T> T convertBytesToObject(byte[] body, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(body, clazz);
        } catch (Exception ex) {
            throw new InvalidRequestBodyException(ex);
        }
    }

    public static <T> T convertStreamToObject(InputStream stream, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(stream, clazz);
        } catch (Exception ex) {
            throw new InvalidRequestBodyException(ex);
        }
    }

}
