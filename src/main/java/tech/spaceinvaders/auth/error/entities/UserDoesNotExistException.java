package tech.spaceinvaders.auth.error.entities;

import static java.lang.String.format;

public class UserDoesNotExistException extends RuntimeException {

    public UserDoesNotExistException(String email) {
        super(format("There is no user with e-mail: %s", email));
    }

}
