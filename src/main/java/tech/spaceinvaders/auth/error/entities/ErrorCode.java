package tech.spaceinvaders.auth.error.entities;

public enum ErrorCode {
    OK(0, "Successful"),
    UNKNOWN(10, "Bad request"),
    NON_EXISTING_RESOURCE(11, "Bad request"),
    BAD_REQUEST(12, "Bad request"),
    INCORRECT_REQUEST_BODY(13, "Bad request"),
    FORBIDDEN(20, "Access denied"),
    BAD_CREDENTIALS(21, "Invalid user or password"),
    DISABLED_USER(22, "This user is disabled");

    private Integer code;
    private String message;

    ErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
