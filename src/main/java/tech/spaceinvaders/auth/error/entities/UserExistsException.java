package tech.spaceinvaders.auth.error.entities;

import static java.lang.String.format;

public class UserExistsException extends RuntimeException {

    public UserExistsException(String email) {
        super(format("Account with e-mail %s is already registered", email));
    }

}
