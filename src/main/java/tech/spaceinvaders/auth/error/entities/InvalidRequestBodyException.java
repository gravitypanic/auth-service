package tech.spaceinvaders.auth.error.entities;

public class InvalidRequestBodyException extends RuntimeException {

    public InvalidRequestBodyException(Throwable cause) {
        super(cause);
    }

    public InvalidRequestBodyException(String message) {
        super(message);
    }

}
