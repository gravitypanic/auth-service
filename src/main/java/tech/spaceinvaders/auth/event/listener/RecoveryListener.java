package tech.spaceinvaders.auth.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import tech.spaceinvaders.auth.event.RecoveryEvent;
import tech.spaceinvaders.auth.mail.Sender;
import tech.spaceinvaders.auth.model.dao.User;
import tech.spaceinvaders.auth.service.UserService;

import java.util.UUID;

@Component
public class RecoveryListener implements ApplicationListener<RecoveryEvent> {

    @Autowired
    private Sender mailSender;

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(RecoveryEvent event) {
        User user = event.getUser();

        String token = UUID.randomUUID().toString();
        userService.removeRecoveryTokenByUser(user);
        userService.addRecoveryToken(user, token);

        mailSender.sendRecoveryMail(user.getEmail(), token);
    }


}
