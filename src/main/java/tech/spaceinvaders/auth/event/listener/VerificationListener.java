package tech.spaceinvaders.auth.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import tech.spaceinvaders.auth.event.VerificationEvent;
import tech.spaceinvaders.auth.mail.Sender;
import tech.spaceinvaders.auth.model.dao.User;
import tech.spaceinvaders.auth.service.UserService;

import java.util.UUID;

@Component
public class VerificationListener implements ApplicationListener<VerificationEvent> {

    @Autowired
    private Sender mailSender;

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(VerificationEvent event) {
        User user = event.getUser();

        String token = UUID.randomUUID().toString();
        userService.removeVerificationTokenByUser(user);
        userService.addVerificationToken(user, token);

        mailSender.sendValidationMail(user.getEmail(), token);
    }

}