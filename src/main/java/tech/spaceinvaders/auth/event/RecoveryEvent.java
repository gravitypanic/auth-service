package tech.spaceinvaders.auth.event;

import org.springframework.context.ApplicationEvent;
import tech.spaceinvaders.auth.model.dao.User;

public class RecoveryEvent extends ApplicationEvent {

    private User user;

    public RecoveryEvent(User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
