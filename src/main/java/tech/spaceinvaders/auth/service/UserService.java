package tech.spaceinvaders.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tech.spaceinvaders.auth.error.entities.InvalidTokenException;
import tech.spaceinvaders.auth.error.entities.UserExistsException;
import tech.spaceinvaders.auth.model.dao.*;
import tech.spaceinvaders.auth.model.dto.*;
import tech.spaceinvaders.auth.repository.*;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Date;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private RecoveryRepository recoveryRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public User registerUser(UserDto userDto) {
        if (findUserByEmail(userDto.getEmail())!=null)
            throw new UserExistsException(userDto.getEmail());
        
        User user = new User();

        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setAuthorities(Collections.singletonList(authorityRepository.findByName("USER")));

        return userRepository.save(user);
    }

    public void changePassword(String password) {
        Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = findUserByEmail(((UserDetails) auth).getUsername());

        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    public boolean validateCurrentPassword(String password) {
        Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!auth.getClass().equals(User.class)) {
            User user = findUserByEmail(((UserDetails) auth).getUsername());
            return passwordEncoder.matches(password, user.getPassword());
        }
        return true;
    }

    public void addVerificationToken(User user, String token) {
        Verification verification = new Verification(user, token);
        verificationRepository.save(verification);
    }

    public void validateVerificationToken(String token) {
        Verification verification = verificationRepository.findByToken(token);

        if (verification == null) throw new InvalidTokenException("Token does not found");

        if (verification.getExpirationDate().before(new Date())) {
            verificationRepository.delete(verification);
            throw new InvalidTokenException("Token is already expired");
        }

        User user = verification.getUser();
        user.setEnabled(true);
        userRepository.save(user);
        verificationRepository.delete(verification);
    }

    public void removeVerificationTokenByUser(User user) {
        Verification verification = verificationRepository.findByUser(user);
        if (verification!=null) verificationRepository.delete(verification);
    }

    public void addRecoveryToken(User user, String token) {
        Recovery recovery = new Recovery(user, token);
        recoveryRepository.save(recovery);
    }

    public void validateRecoveryToken(String token) {
        Recovery recovery = recoveryRepository.findByToken(token);

        if (recovery == null) throw new InvalidTokenException("Token does not exist");

        if (recovery.getExpirationDate().before(new Date())) {
            recoveryRepository.delete(recovery);
            throw new InvalidTokenException("Token is already expired");
        }

        User user = recovery.getUser();
        Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
                Collections.singletonList(new SimpleGrantedAuthority("CHANGE_PASSWORD")));
        SecurityContextHolder.getContext().setAuthentication(auth);
        recoveryRepository.delete(recovery);
    }

    public void removeRecoveryTokenByUser(User user) {
        Recovery recovery = recoveryRepository.findByUser(user);
        if (recovery!=null) recoveryRepository.delete(recovery);
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}