package tech.spaceinvaders.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.spaceinvaders.auth.model.dao.Recovery;
import tech.spaceinvaders.auth.model.dao.User;

public interface RecoveryRepository extends JpaRepository<Recovery, Long> {

    Recovery findByToken(String token);

    Recovery findByUser(User user);

}
