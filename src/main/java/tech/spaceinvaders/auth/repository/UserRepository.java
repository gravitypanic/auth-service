package tech.spaceinvaders.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.spaceinvaders.auth.model.dao.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

}
