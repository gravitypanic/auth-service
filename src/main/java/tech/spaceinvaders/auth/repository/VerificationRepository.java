package tech.spaceinvaders.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.spaceinvaders.auth.model.dao.User;
import tech.spaceinvaders.auth.model.dao.Verification;

public interface VerificationRepository extends JpaRepository<Verification, Long> {

    Verification findByToken(String token);

    Verification findByUser(User user);
}
