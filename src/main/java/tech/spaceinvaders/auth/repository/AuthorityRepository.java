package tech.spaceinvaders.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.spaceinvaders.auth.model.dao.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByName(String name);

}
