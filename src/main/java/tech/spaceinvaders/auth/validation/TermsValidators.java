package tech.spaceinvaders.auth.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TermsValidators implements ConstraintValidator<ValidateTerms, Boolean> {

    @Override
    public boolean isValid(Boolean terms, ConstraintValidatorContext context) {
        return terms == null ? false : terms;
    }

}
