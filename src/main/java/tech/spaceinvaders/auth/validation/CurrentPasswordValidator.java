package tech.spaceinvaders.auth.validation;

import org.springframework.beans.factory.annotation.Autowired;
import tech.spaceinvaders.auth.service.UserService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CurrentPasswordValidator implements ConstraintValidator<ValidateCurrentPassword, String> {

    @Autowired
    private UserService userService;

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        return userService.validateCurrentPassword(password);
    }

}
