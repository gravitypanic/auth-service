package tech.spaceinvaders.auth.validation;

import tech.spaceinvaders.auth.model.dto.UserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatcher implements ConstraintValidator<MatchPassword, Object> {

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        UserDto user = (UserDto) obj;
        return user.getPassword().equals(user.getDuplicatedPassword());
    }
}
