package tech.spaceinvaders.auth.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<ValidatePassword, String> {

    private static final String STRONG_PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[^\\w\\s]).{8,40})";
    private static final String WEAK_PASSWORD_PATTERN = "((?=.*[a-zA-Z0-9]).{3,40})";
    private static final Pattern PATTEN = Pattern.compile(WEAK_PASSWORD_PATTERN);

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        return PATTEN.matcher(password).matches();
    }

}