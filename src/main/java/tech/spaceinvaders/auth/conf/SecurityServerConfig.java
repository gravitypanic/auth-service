package tech.spaceinvaders.auth.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import tech.spaceinvaders.auth.conf.authentication.AuthEntryPoint;
import tech.spaceinvaders.auth.conf.authentication.AuthFilter;
import tech.spaceinvaders.auth.conf.authentication.handler.LoginFailureHandler;
import tech.spaceinvaders.auth.conf.authentication.handler.LoginSuccessHandler;
import tech.spaceinvaders.auth.conf.authentication.handler.LogoutHandler;

import javax.sql.DataSource;

@Configuration
public class SecurityServerConfig extends WebSecurityConfigurerAdapter {

    private static final String USER_QUERY = "select username, password, enabled from users where username=?";
    private static final String AUTHORITY_QUERY = "select U.username, A.name FROM users U JOIN users_authorities UA " +
            "ON U.id = UA.user_id JOIN authorities A ON UA.authority_id = A.id WHERE U.username=?";
    private static final String[] NO_AUTH_REQUIRED_PATHS = {"/registration", "/registration/confirm*", "/login*", "/recovery*",
            "/registration/confirm/resend*", "/favicon.ico", "/validation*", "/user/recovery*", "/user/recovery/validate*"};
    private static final String[] CHANGE_PASSWORD_PATHS = {"/password*", "/user/password/change*"};
    private static final String[] CHANGE_PASSWORD_AUTHORITIES = {"CHANGE_PASSWORD", "USER"};
    private static final String[] ANY_REQUEST_AUTHORITIES = {"USER"};

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private LoginSuccessHandler successHandler;
    @Autowired
    private LoginFailureHandler failureHandler;
    @Autowired
    private LogoutHandler logoutHandler;
    @Autowired
    private AuthEntryPoint authEntryPoint;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery(USER_QUERY)
                .authoritiesByUsernameQuery(AUTHORITY_QUERY)
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .addFilterAt(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                    .antMatchers(NO_AUTH_REQUIRED_PATHS)
                        .permitAll()
                    .antMatchers(CHANGE_PASSWORD_PATHS)
                        .hasAnyAuthority(CHANGE_PASSWORD_AUTHORITIES)
                    .anyRequest()
                        .hasAnyAuthority(ANY_REQUEST_AUTHORITIES)
                .and()
                    .exceptionHandling()
                        .authenticationEntryPoint(authEntryPoint)
                .and()
                    .logout()
                        .addLogoutHandler(logoutHandler)
                        .clearAuthentication(true)
                .and()
                    .csrf()
                    .disable();
    }

    private AuthFilter authenticationFilter() throws Exception {
        AuthFilter filter = new AuthFilter();
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.setAuthenticationSuccessHandler(successHandler);
        filter.setAuthenticationFailureHandler(failureHandler);
        return filter;
    }

}
