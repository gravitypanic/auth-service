package tech.spaceinvaders.auth.conf.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.util.ContentCachingRequestWrapper;
import tech.spaceinvaders.auth.model.dto.UserDto;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static tech.spaceinvaders.auth.utils.InputHandler.*;

public class AuthFilter extends AbstractAuthenticationProcessingFilter {

    public AuthFilter() {
        super(new AntPathRequestMatcher("/login", "POST"));
    }

    @Override
    public void doFilter(ServletRequest originalRequest, ServletResponse originalResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) originalRequest;
        HttpServletResponse response = (HttpServletResponse) originalResponse;

        if (!requiresAuthentication(request, response)) {
            chain.doFilter(request, response);
            return;
        }

        Authentication authResult;

        try {
            authResult = attemptAuthentication(request, response);
            if (authResult == null) return;
        } catch (AuthenticationException failed) {
            unsuccessfulAuthentication(new ContentCachingRequestWrapper(request), response, failed);
            return;
        }

        successfulAuthentication(request, response, chain, authResult);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        UserDto user = getBody(request);
        return this.getAuthenticationManager()
                .authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
    }

    private UserDto getBody(HttpServletRequest request) {
        try {
            return convertStreamToObject(request.getInputStream(), UserDto.class);
        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

}
