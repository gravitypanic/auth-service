package tech.spaceinvaders.auth.conf.authentication.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import tech.spaceinvaders.auth.model.dto.common.DefaultResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LogoutHandler extends SecurityContextLogoutHandler {

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication)  {
        super.logout(request, response, authentication);
        try {
            setBody(response, new DefaultResponse("successful"));
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    private void setBody(HttpServletResponse response, Object body) throws IOException {
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.getWriter().flush();
    }

}
