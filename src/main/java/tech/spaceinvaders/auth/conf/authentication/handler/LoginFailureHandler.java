package tech.spaceinvaders.auth.conf.authentication.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import tech.spaceinvaders.auth.error.entities.ErrorCode;
import tech.spaceinvaders.auth.model.dto.UserDto;
import tech.spaceinvaders.auth.model.dto.common.DefaultResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static tech.spaceinvaders.auth.utils.InputHandler.*;

@Component
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
                                        final AuthenticationException exception) throws IOException {
        DefaultResponse defaultResponse;

        if (exception instanceof DisabledException) {
            defaultResponse = new DefaultResponse(ErrorCode.DISABLED_USER, getBody(request).getEmail());
        } else
            if (exception instanceof BadCredentialsException) {
                defaultResponse = new DefaultResponse(ErrorCode.BAD_CREDENTIALS);
            } else {
                defaultResponse = new DefaultResponse(ErrorCode.UNKNOWN);
            }

        setBody(response, defaultResponse);
    }

    private UserDto getBody(HttpServletRequest request) {
        return convertBytesToObject(((ContentCachingRequestWrapper)request).getContentAsByteArray(), UserDto.class);
    }

    private void setBody(HttpServletResponse response, Object body) throws IOException {
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.getWriter().flush();
    }

}