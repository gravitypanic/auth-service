package tech.spaceinvaders.auth.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    private static final String VERIFICATION_EMAIL_SUBJECT = "Verification email";
    private static final String VERIFICATION_EMAIL_TEXT = "To verify your email address, please, follow link below." +
            "\nThis link will be available during 24h.\n";
    private static final String RECOVERY_EMAIL_SUBJECT = "Password recovery email";
    private static final String RECOVERY_EMAIL_TEXT = "To reset your password, please, follow link below." +
            "\nThis link will be available during 24h.\n";

    @Value("${server.address}")
    private String hostname;

    @Value("${spring.mail.username}")
    private String fromAddress;

    @Autowired
    private JavaMailSender sender;

    public void sendValidationMail(String address, String token) {
        String validationUrl = "http://".concat(hostname.concat("/validation?token=").concat(token));
        send(buildMail(address, VERIFICATION_EMAIL_SUBJECT, VERIFICATION_EMAIL_TEXT + validationUrl));
    }

    public void sendRecoveryMail(String address, String token) {
        String recoveryUrl = "http://".concat(hostname.concat("/recovery?token=")).concat(token);
        send(buildMail(address, RECOVERY_EMAIL_SUBJECT, RECOVERY_EMAIL_TEXT + recoveryUrl));
    }

    private SimpleMailMessage buildMail(String address, String subject, String text) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(address);
        mail.setFrom(fromAddress);
        mail.setSubject(subject);
        mail.setText(text);
        return mail;
    }

    private void send(SimpleMailMessage mail) {
        try {
            sender.send(mail);
        } catch (MailSendException ignore) {}
        //TODO: add handler
    }

}
